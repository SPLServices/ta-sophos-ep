Sophos Technology Add-on
----------------------------------------
	Author: Splunk Professional Services
	Built in cooperation with: Sophos HQ, UK
	Sophos Contact: John Stringer, Product Manager, Sophos. john dot stringer at sophos dot com
	Version/Date: v1.0 7/18/2012 21:00 pm

	Source type(s): sophos:malware, sophos:status, sophos:update
	Input requirements: Unlike the previous TA-sophos, this TA relies on Sophos data from the Sophos 
	                    Reporting Interface (SRI) coming in over syslog from one of the supported 
	                    products below. Ensure proper sourcetypes are assigned, see Using section below.
	Has index-time operations: true, this TA must be placed on indexers

	Supported product(s): (Splunk for Enterprise Security Dashboards supported in parentheses)

		 * Sophos Endpoint Protection (also called SEC for Sophos Enterprise Console)
		   sophos:sec sourcetype
		   -- Endpoint Antivirus and HIPS (Malware)
		   -- Endpoint Firewall (Traffic)
		 * Sophos Web Appliance
		   sophos:swa sourcetype
		   -- Web Proxy logs (Proxy)
		   -- Web filtering events (Malware)
		 * Sophos Universal Traffic Monitor
		   sophos:utm:* sourcetypes
		   -- Network Security (Traffic)
		   -- Intrusion Prevention System (Intrusion)
		   -- VPN Gateway (Access, Session)

	To Do: 
		 * Sophos Endpoint Protection (also called SEC for Sophos Enterprise Console)
		   -- Identify and normalize Endpoint Software Version / Updates data (Malware Operations)
		   -- Identify and normalize Patch, Policy (Endpoint/Application/Web/Data Control) data
		 * Sophos Web Appliance
		   -- Identify and normalize Application/Wireless Control, Web/Email Filtering data
		 * Sophos PureMessage for Exhange data
		 * Sophos Email Appliance data
		 * Sophos SafeGuard Enterprise data
		 * Sophos Mobile Control data


Using this Technology Add-on:
----------------------------------------
	Configuration: Manual
	Ports for automatic configuration: None
	Scripted input setup: None
	Custom Props/Transforms: Enable the force_sourcetype_for_utm_firewall, force_sourcetype_for_utm_ips
	                        and force_sourcetype_for_utm_ipsec transforms against the syslog stream
	                        through which the UTM data is coming in - this should filter the UTM data
	                        out into its own sourcetypes used by the TA. Apply the all_sourcetype_sec 
	                        or all_sourcetype_swa transforms to specific hosts or sources of data, as
	                        appropriate, to force the entire stream of data to Sophos SEC or SWA 
	                        sourcetypes used by the TA.
